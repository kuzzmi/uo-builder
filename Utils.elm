module Utils exposing (indexOf)


indexOf : a -> List a -> Maybe Int
indexOf a list =
    let
        indexedList =
            List.indexedMap (,) list

        filter ( index, item ) =
            if item == a then
                Just index
            else
                Nothing

        filteredSkills =
            List.filterMap filter indexedList
    in
    List.head filteredSkills
