module Page.Build exposing (Model, Msg, init, update, view)

import Data.Build exposing (Build)
import Data.Skill exposing (Skill(..))
import Data.Spell exposing (Spell(..))
import Element exposing (..)
import Element.Attributes exposing (..)
import Element.Events as Events
import Element.Input as Input
import Feather
import Styles exposing (..)


type Msg
    = UpdateSkillsSelector (Input.SelectMsg Skill)
    | UpdateBuild BuildMsg


type BuildMsg
    = AddSkill (Maybe Skill)
    | RemoveSkill Int
    | UpdateSkillPercentage Int String
    | UpdateStr String
    | UpdateDex String
    | UpdateInt String


type alias Model =
    { build : Build
    , selectedSkill : Maybe Skill
    , skillsDropdown : Input.SelectWith Skill Msg
    }


init : Model
init =
    { build = Data.Build.new
    , selectedSkill = Nothing
    , skillsDropdown = Input.autocomplete Nothing UpdateSkillsSelector
    }


normalizeValue : String -> Int
normalizeValue value =
    let
        value_ =
            Result.withDefault 0 (String.toInt value)
    in
    if value_ > 100 then
        100
    else
        value_


updateBuild : BuildMsg -> Build -> Build
updateBuild action build =
    case action of
        AddSkill selected ->
            case ( selected, build.skills ) of
                ( Just skill, [] ) ->
                    { build
                        | skills = [ ( skill, 100 ) ]
                    }

                ( Just skill, skills ) ->
                    { build
                        | skills = skills ++ [ ( skill, 100 ) ]
                    }

                ( Nothing, _ ) ->
                    build

        RemoveSkill index ->
            let
                filterIndexedSkill ( index_, skill ) =
                    if index_ == index then
                        Nothing
                    else
                        Just skill

                updatedSkills =
                    List.indexedMap (,) build.skills
                        |> List.filterMap filterIndexedSkill
            in
            { build | skills = updatedSkills }

        UpdateSkillPercentage index value ->
            let
                value_ =
                    normalizeValue value

                updateSkill index_ ( skill, percentage ) =
                    if index == index_ then
                        ( skill, value_ )
                    else
                        ( skill, percentage )

                updatedSkills =
                    List.indexedMap updateSkill build.skills
            in
            { build | skills = updatedSkills }

        UpdateStr value ->
            let
                value_ =
                    normalizeValue value

                ( _, dex, int ) =
                    build.stats
            in
            { build | stats = ( value_, dex, int ) }

        UpdateDex value ->
            let
                value_ =
                    normalizeValue value

                ( str, _, int ) =
                    build.stats
            in
            { build | stats = ( str, value_, int ) }

        UpdateInt value ->
            let
                value_ =
                    normalizeValue value

                ( str, dex, _ ) =
                    build.stats
            in
            { build | stats = ( str, dex, value_ ) }


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case action of
        UpdateSkillsSelector selectionMsg ->
            let
                newState =
                    Input.updateSelection
                        selectionMsg
                        model.skillsDropdown

                selectedSkill =
                    Input.selected newState
            in
            ( { model
                | skillsDropdown = newState
                , selectedSkill = selectedSkill
              }
            , Cmd.none
            )

        UpdateBuild buildMsg ->
            let
                newBuild =
                    updateBuild buildMsg model.build
            in
            case buildMsg of
                AddSkill _ ->
                    ( { model
                        | build = newBuild
                        , skillsDropdown = Input.clear model.skillsDropdown
                        , selectedSkill = Nothing
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | build = newBuild }, Cmd.none )


viewStats : ( Int, Int, Int ) -> Element Styles variation Msg
viewStats ( str, dex, int ) =
    let
        hp =
            50 + floor (toFloat str / 2)

        statsRow label1 label2 value1 value2 msg =
            row None
                [ spacing 10, verticalCenter ]
                [ Input.text Field
                    [ paddingXY 10 5, width <| px 50 ]
                    { onChange = UpdateBuild << msg
                    , value = toString value1
                    , label = Input.hiddenLabel ""
                    , options = []
                    }
                , el None [ width <| px 150 ] <| text label1
                , el None [ width <| px 100 ] <| text <| label2 ++ ": " ++ toString value2
                ]
    in
    column None
        [ spacing 10 ]
        [ statsRow "Strength" "HP" str hp UpdateStr
        , statsRow "Dexterity" "Stamina" dex dex UpdateDex
        , statsRow "Intelligence" "Mana" int int UpdateInt
        ]


viewSkills : List ( Skill, Int ) -> Element Styles variation Msg
viewSkills skills =
    let
        viewSkill index ( skill, perc ) =
            row None
                [ spacing 10, verticalCenter ]
                [ Input.text Field
                    [ paddingXY 10 5, width <| px 50 ]
                    { onChange = UpdateBuild << UpdateSkillPercentage index
                    , value = toString perc
                    , label = Input.hiddenLabel ""
                    , options = []
                    }
                , el None [ width <| px 200 ] <|
                    text (Data.Skill.toString skill)
                , button Button
                    [ Events.onClick (UpdateBuild (RemoveSkill index))
                    , paddingXY 10 6
                    ]
                    (html <| Feather.trash "#384047" 14)
                ]
    in
    if List.length skills == 0 then
        el Muted [] <| text "No skills selected yet"
    else
        column None [ spacing 10, center ] (List.indexedMap viewSkill skills)


viewSkillsSummary : List ( Skill, Int ) -> Element Styles variation Msg
viewSkillsSummary skills =
    let
        totalSkillPoints =
            List.foldr (+) 0 <| List.map Tuple.second skills
    in
    text <| "Total: " ++ toString totalSkillPoints ++ " / 700"


viewSkillsDropdown : Model -> Element Styles variation Msg
viewSkillsDropdown model =
    let
        skills =
            List.map Tuple.first model.build.skills

        choice skill =
            if List.member skill skills then
                Nothing
            else
                Just <| Input.choice skill (text <| Data.Skill.toString skill)

        choices =
            List.filterMap choice Data.Skill.list
    in
    row None
        [ spacing 10, verticalCenter ]
        [ text "Skills"
        , Input.select
            Field
            [ padding 5, spacing 10 ]
            { label = Input.hiddenLabel ""
            , with = model.skillsDropdown
            , max = 5
            , options = []
            , menu =
                Input.menu SubMenu
                    [ maxHeight <| px 400, scrollbars ]
                    choices
            }
        , button Button
            [ Events.onClick <| UpdateBuild <| AddSkill model.selectedSkill
            , paddingXY 10 6
            ]
            (text "Add skill")
        ]


viewStatsSummary : Build -> Element Styles variation Msg
viewStatsSummary build =
    let
        ( str, dex, int ) =
            build.stats

        total =
            str + dex + int

        hp =
            50 + floor (toFloat str / 2)
    in
    column None
        [ spacing 10 ]
        [ text <| "Total: " ++ toString total ++ " / 225"
        ]


viewChecklist : Build -> Element Styles variation Msg
viewChecklist build =
    let
        ( str, dex, int ) =
            build.stats

        magerySkill =
            Data.Skill.getSkillValue Magery build.skills

        castsSpell spell =
            Data.Spell.spellCastChance int magerySkill spell > 0

        castsRecall =
            castsSpell Recall

        castsGateTravel =
            castsSpell GateTravel

        castsMark =
            castsSpell Mark

        checkListItem label checked =
            let
                icon =
                    if checked then
                        html <| Feather.check "#8BC34A" 14
                    else
                        html <| Feather.x "#F44336" 14
            in
            row None
                [ spacing 10, verticalCenter ]
                [ icon, text label ]

        anatomySkill =
            Data.Skill.getSkillValue Anatomy build.skills

        healingSkill =
            Data.Skill.getSkillValue Healing build.skills

        healingMin =
            anatomySkill // 5 + healingSkill // 5 + 3

        healingMax =
            anatomySkill // 5 + healingSkill // 2 + 10

        healingTime =
            if dex > 80 then
                10 - ((dex - 20) // 20)
            else
                10

        canCure =
            if healingSkill > 60 then
                True
            else
                False

        canRessurect =
            if healingSkill > 80 then
                True
            else
                False

        wrestlingSkill =
            Data.Skill.getSkillValue Wrestling build.skills

        armsSkill =
            Data.Skill.getSkillValue ArmsLore build.skills

        evalSkill =
            Data.Skill.getSkillValue EvaluatingIntelligence build.skills

        tacticsSkill =
            Data.Skill.getSkillValue Tactics build.skills

        ljSkill =
            Data.Skill.getSkillValue Lumberjacking build.skills

        damageIncrease =
            let
                anatomyIncrease =
                    anatomySkill // 10

                baseIncrease =
                    tacticsSkill + 50
            in
            anatomyIncrease + baseIncrease

        ljDamageIncrease =
            let
                base =
                    ljSkill // 10 - 2
            in
            if ljSkill == 100 then
                base + 1
            else if ljSkill < 20 then
                0
            else
                base

        specialChance =
            anatomySkill // 4

        canStun =
            wrestlingSkill > 80 && anatomySkill > 80

        canDisarm =
            wrestlingSkill > 80 && armsSkill > 80

        deffensiveWrestle =
            (anatomySkill + evalSkill) // 2
    in
    column None
        [ spacing 5 ]
        [ checkListItem "Casts Recall" castsRecall
        , checkListItem "Casts Gate Travel" castsGateTravel
        , checkListItem "Casts Mark" castsMark
        , checkListItem "Disarms" canDisarm
        , checkListItem "Stuns" canStun
        , text <| "Deffensive wrestling: " ++ toString deffensiveWrestle ++ "%"
        , text <| "Special move chance: " ++ toString specialChance ++ "%"
        , checkListItem "Cures (bandages)" canCure
        , checkListItem "Ressurects (bandages)" canRessurect
        , text <| "Min/Max HP healed: " ++ toString healingMin ++ "-" ++ toString healingMax
        , text <| "Healing speed: " ++ toString healingTime ++ "s"
        , text <| "Damage increase: " ++ toString damageIncrease ++ "%"
        , text <| "LJ damage increase: " ++ toString ljDamageIncrease
        ]


viewMageryChecklist : Build -> Element Styles variation Msg
viewMageryChecklist build =
    let
        ( _, _, int ) =
            build.stats

        magerySkill =
            Data.Skill.getSkillValue Magery build.skills

        isMagery ( skill, _ ) =
            skill == Magery

        circleChance =
            Data.Spell.circleCastChance int magerySkill

        circleText c =
            text <| Data.Spell.toString c

        manaReqText c =
            text <| toString <| Data.Spell.circleManaReq c

        mageryReqText c =
            text <| toString <| Data.Spell.circleMageryReq c

        chanceText c =
            let
                chance =
                    circleChance c
            in
            checkListItem (toString chance ++ "%") chance

        checkListItem label chance =
            let
                icon =
                    if chance > 0 then
                        html <| Feather.check "#8BC34A" 14
                    else
                        html <| Feather.x "#F44336" 14
            in
            row None
                [ spacing 10, verticalCenter ]
                [ icon, text label ]

        header label =
            el TableHeader [] <| text label
    in
    column None
        [ spacing 10 ]
        [ table None
            []
            [ header "Circle" :: List.map circleText Data.Spell.list
            , header "Min Mana" :: List.map manaReqText Data.Spell.list
            , header "Min Magery" :: List.map mageryReqText Data.Spell.list
            , header "Cast Chance" :: List.map chanceText Data.Spell.list
            ]
        ]


view : Model -> Element Styles variation Msg
view model =
    column None
        [ spacing 20 ]
        [ row None
            [ spacing 20 ]
            [ section Section [ padding 20, width <| percent 33.3 ] <|
                column None
                    [ spacing 20 ]
                    [ h3 SubTitle [] (text "Skills")
                    , viewSkills model.build.skills
                    , viewSkillsSummary model.build.skills
                    , viewSkillsDropdown model
                    ]
            , section Section [ padding 20, width <| percent 33.3 ] <|
                column None
                    [ spacing 20 ]
                    [ h3 SubTitle [] (text "Stats")
                    , viewStats model.build.stats
                    , viewStatsSummary model.build
                    ]
            , section Section [ padding 20, width <| percent 33.3 ] <|
                column None
                    [ spacing 20 ]
                    [ h3 SubTitle [] (text "Misc")
                    , viewChecklist model.build
                    ]
            ]
        , section Section [ padding 20 ] <|
            column None
                [ spacing 20 ]
                [ h3 SubTitle [] <| text "Spell Casting"
                , viewMageryChecklist model.build
                ]
        ]



-- viewHotlink : Model -> Element Styles variation Msg
-- viewHotlink model =
--     let
--         skillPairText ( skill, perc ) result =
--             let
--                 index =
--                     Utils.indexOf skill Skill.list
--
--                 text =
--                     case index of
--                         Just index_ ->
--                             toString index_ ++ "." ++ toString perc ++ "|"
--
--                         Nothing ->
--                             ""
--             in
--             result ++ text
--
--         encodedSkills =
--             List.foldr skillPairText "" model.skills
--     in
--     text encodedSkills
