const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: [
        path.join(__dirname, './index.js'),
    ],
    output: {
        path: path.join(__dirname, './public'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js', '.elm'],
    },
    module: {
        rules: [{
            test: /\.elm$/,
            exclude: [
                /elm-stuff/,
                /node_modules/,
            ],
            loader: 'elm-webpack-loader?maxInstances=2',
        }],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
        new UglifyJsPlugin(),
    ],
};
