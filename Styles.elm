module Styles exposing (..)

import Color
import Style exposing (style)
import Style.Border as Border
import Style.Color as Color
import Style.Font as Font
import Style.Shadow as Shadow


-- Styles


type Styles
    = Page
    | Title
    | SubTitle
    | Field
    | Muted
    | SubMenu
    | Button
    | ButtonTransparent
    | TableHeader
    | Section
    | None



-- We define our stylesheet


stylesheet =
    Style.styleSheet
        [ style Title
            [ Font.size 28
            , Font.weight 700
            ]
        , style SubTitle
            [ Font.size 20
            , Font.weight 700
            ]
        , style TableHeader
            [ Font.weight 700
            ]
        , style Page
            [ Color.text Color.darkCharcoal
            , Color.background Color.white
            , Font.size 16
            , Font.lineHeight 1.3
            , Font.typeface
                [ Font.font "helvetica"
                , Font.font "arial"
                , Font.font "sans-serif"
                ]
            ]
        , style Section
            [ Border.rounded 6
            , Border.all 1
            , Border.solid
            , Color.border Color.lightGrey
            , Shadow.glow Color.lightGrey 2
            ]
        , style Muted
            [ Color.text Color.darkGrey
            , Font.italic
            ]
        , style Field
            [ Border.rounded 6
            , Border.all 1
            , Border.solid
            , Color.border Color.lightGrey
            ]
        , style SubMenu
            [ Border.rounded 6
            , Border.all 2
            , Border.solid
            , Color.border Color.lightGrey
            ]
        , style ButtonTransparent
            [ Border.rounded 6
            , Color.background <| Color.rgba 0 0 0 0
            ]
        , style Button
            [ Border.rounded 6
            , Border.all 1
            , Border.solid
            , Color.border Color.lightGrey
            ]
        , style None []
        ]
