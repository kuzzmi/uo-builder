const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:8000',
        'webpack/hot/only-dev-server',
        path.join(__dirname, './index.js'),
    ],
    output: {
        path: path.join(__dirname, './public'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    devServer: {
        hot: true,
        contentBase: path.join(__dirname, './public'),
        publicPath: '/',
        historyApiFallback: true,
    },
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.js', '.elm'],
    },
    module: {
        rules: [{
            test: /\.elm$/,
            exclude: [/elm-stuff/, /node_modules/],
            use: {
                loader: 'elm-webpack-loader',
                options: {}
            }
        }]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
};
