port module Main exposing (..)

import Color
import Element exposing (..)
import Element.Attributes exposing (..)
import Element.Events as Events
import Element.Input as Input
import Feather
import Html exposing (Html, program)
import Json.Decode as Decode
import Page.Build as Build
import Styles exposing (..)
import Utils


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


type Page
    = NewBuild Build.Model



-- | NewAccount
-- | EditBuild Int
-- | EditAccount Int


type alias Model =
    { page : Page
    }


type Msg
    = UpdateBuild Build.Msg


initModel : Model
initModel =
    { page = NewBuild Build.init
    }


init : ( Model, Cmd Msg )
init =
    ( initModel, Cmd.none )


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case ( action, model.page ) of
        ( UpdateBuild buildMsg, NewBuild page ) ->
            let
                ( page_, cmd ) =
                    Build.update buildMsg page
            in
            ( { model | page = NewBuild page_ }, Cmd.map UpdateBuild cmd )


viewPage : Page -> Element Styles variation Msg
viewPage page =
    case page of
        NewBuild page_ ->
            Build.view page_ |> Element.map UpdateBuild


view : Model -> Html Msg
view model =
    Element.viewport stylesheet <|
        el None [ center, paddingXY 20 0, width fill ] <|
            column Page
                [ spacing 20 ]
                [ h1 Title [ paddingXY 0 20 ] (text "UO Builder")
                , viewPage model.page
                ]
