module Data.Skill exposing (Skill(..), getSkillValue, list, toString)


type Skill
    = Alchemy
    | Anatomy
    | AnimalLore
    | AnimalTaming
    | Archery
    | ArmsLore
    | Begging
    | Blacksmithy
    | Bowcraft
    | Camping
    | Carpentry
    | Cartography
    | Cooking
    | DetectingHidden
    | Discordance
    | EvaluatingIntelligence
    | Fencing
    | Fishing
    | ForensicEvaluation
    | Healing
    | Herding
    | Hiding
    | Inscription
    | ItemIdentification
    | Lockpicking
    | Lumberjacking
    | MaceFighting
    | Magery
    | Meditation
    | Mining
    | Musicianship
    | Parrying
    | Peacemaking
    | Poisoning
    | Provocation
    | RemoveTrap
    | Resisting
    | Snooping
    | SpiritSpeak
    | Stealing
    | Stealth
    | Swordsmanship
    | Tactics
    | Tailoring
    | TasteIdentification
    | Tinkering
    | Tracking
    | Veterinary
    | Wrestling


list : List Skill
list =
    [ Alchemy
    , Anatomy
    , AnimalLore
    , AnimalTaming
    , Archery
    , ArmsLore
    , Begging
    , Blacksmithy
    , Bowcraft
    , Camping
    , Carpentry
    , Cartography
    , Cooking
    , DetectingHidden
    , Discordance
    , EvaluatingIntelligence
    , Fencing
    , Fishing
    , ForensicEvaluation
    , Healing
    , Herding
    , Hiding
    , Inscription
    , ItemIdentification
    , Lockpicking
    , Lumberjacking
    , MaceFighting
    , Magery
    , Meditation
    , Mining
    , Musicianship
    , Parrying
    , Peacemaking
    , Poisoning
    , Provocation
    , RemoveTrap
    , Resisting
    , Snooping
    , SpiritSpeak
    , Stealing
    , Stealth
    , Swordsmanship
    , Tactics
    , Tailoring
    , TasteIdentification
    , Tinkering
    , Tracking
    , Veterinary
    , Wrestling
    ]


toString : Skill -> String
toString skill =
    case skill of
        Alchemy ->
            "Alchemy"

        Anatomy ->
            "Anatomy"

        AnimalLore ->
            "Animal Lore"

        AnimalTaming ->
            "Animal Taming"

        Archery ->
            "Archery"

        ArmsLore ->
            "Arms Lore"

        Begging ->
            "Begging"

        Blacksmithy ->
            "Blacksmithy"

        Bowcraft ->
            "Bowcraft"

        Camping ->
            "Camping"

        Carpentry ->
            "Carpentry"

        Cartography ->
            "Cartography"

        Cooking ->
            "Cooking"

        DetectingHidden ->
            "Detecting Hidden"

        Discordance ->
            "Discordance"

        EvaluatingIntelligence ->
            "Evaluating Intelligence"

        Fencing ->
            "Fencing"

        Fishing ->
            "Fishing"

        ForensicEvaluation ->
            "Forensic Evaluation"

        Healing ->
            "Healing"

        Herding ->
            "Herding"

        Hiding ->
            "Hiding"

        Inscription ->
            "Inscription"

        ItemIdentification ->
            "Item Identification"

        Lockpicking ->
            "Lockpicking"

        Lumberjacking ->
            "Lumberjacking"

        MaceFighting ->
            "Mace Fighting"

        Magery ->
            "Magery"

        Meditation ->
            "Meditation"

        Mining ->
            "Mining"

        Musicianship ->
            "Musicianship"

        Parrying ->
            "Parrying"

        Peacemaking ->
            "Peacemaking"

        Poisoning ->
            "Poisoning"

        Provocation ->
            "Provocation"

        RemoveTrap ->
            "RemoveTrap"

        Resisting ->
            "Resisting"

        Snooping ->
            "Snooping"

        SpiritSpeak ->
            "Spirit Speak"

        Stealing ->
            "Stealing"

        Stealth ->
            "Stealth"

        Swordsmanship ->
            "Swordsmanship"

        Tactics ->
            "Tactics"

        Tailoring ->
            "Tailoring"

        TasteIdentification ->
            "Taste Identification"

        Tinkering ->
            "Tinkering"

        Tracking ->
            "Tracking"

        Veterinary ->
            "Veterinary"

        Wrestling ->
            "Wrestling"


getSkillValue : Skill -> List ( Skill, Int ) -> Int
getSkillValue skill skills =
    let
        found ( skill_, _ ) =
            skill == skill_

        ( _, value ) =
            List.filter found skills
                |> List.head
                |> Maybe.withDefault ( skill, 0 )
    in
    value
