module Data.Build exposing (Build, new)

import Data.Skill exposing (Skill)


type alias Build =
    { id : Int
    , name : String
    , skills : List ( Skill, Int )
    , stats : ( Int, Int, Int )
    }


new : Build
new =
    { id = 0
    , name = ""
    , skills = []
    , stats = ( 60, 10, 10 )
    }
