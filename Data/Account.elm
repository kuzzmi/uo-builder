module Data.Account exposing (Account, new)

import Data.Build exposing (Build)


type alias Account =
    { id : Int
    , name : String
    , builds : List Build
    }


new : Account
new =
    { id = 0
    , name = ""
    , builds = []
    }
