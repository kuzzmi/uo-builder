module Data.Spell
    exposing
        ( Circle(..)
        , Spell(..)
        , circleCastChance
        , circleMageryReq
        , circleManaReq
        , list
        , spellCastChance
        , toString
        )


type Spell
    = Clumsy
    | CreateFood
    | Feeblemind
    | Heal
    | MagicArrow
    | NightSight
    | ReactiveArmor
    | Weaken
      -- 2nd
    | Agility
    | Cunning
    | Cure
    | Harm
    | MagicTrap
    | MagicUntrap
    | Protection
    | Strength
      -- 3rd
    | Bless
    | Fireball
    | MagicLock
    | Poison
    | Telekinesis
    | Teleport
    | Unlock
    | WallOfStone
      -- 4th
    | ArchCure
    | ArchProtection
    | Curse
    | FireField
    | GreaterHeal
    | Lightning
    | ManaDrain
    | Recall
      -- 5th
    | BladeSpirits
    | DispelField
    | Incognito
    | MagicReflection
    | MindBlast
    | Paralyze
    | PoisonField
    | SummonCreature
      -- 6th
    | Dispel
    | EnergyBolt
    | Explosion
    | Invisibility
    | Mark
    | MassCurse
    | ParalyzeField
    | Reveal
      -- 7th
    | ChainLightning
    | EnergyField
    | Flamestrike
    | GateTravel
    | ManaVampire
    | MassDispel
    | MeteorSwarm
    | Polymorph
      -- 8th
    | Earthquake
    | EnergyVortex
    | Resurrection
    | SummonAirElemental
    | SummonDaemon
    | SummonEarthElemental
    | SummonFireElemental
    | SummonWaterElemental


getSpellCircle : Spell -> Circle
getSpellCircle spell =
    case spell of
        Clumsy ->
            First

        CreateFood ->
            First

        Feeblemind ->
            First

        Heal ->
            First

        MagicArrow ->
            First

        NightSight ->
            First

        ReactiveArmor ->
            First

        Weaken ->
            First

        Agility ->
            Second

        Cunning ->
            Second

        Cure ->
            Second

        Harm ->
            Second

        MagicTrap ->
            Second

        MagicUntrap ->
            Second

        Protection ->
            Second

        Strength ->
            Second

        Bless ->
            Third

        Fireball ->
            Third

        MagicLock ->
            Third

        Poison ->
            Third

        Telekinesis ->
            Third

        Teleport ->
            Third

        Unlock ->
            Third

        WallOfStone ->
            Third

        ArchCure ->
            Fourth

        ArchProtection ->
            Fourth

        Curse ->
            Fourth

        FireField ->
            Fourth

        GreaterHeal ->
            Fourth

        Lightning ->
            Fourth

        ManaDrain ->
            Fourth

        Recall ->
            Fourth

        BladeSpirits ->
            Fifth

        DispelField ->
            Fifth

        Incognito ->
            Fifth

        MagicReflection ->
            Fifth

        MindBlast ->
            Fifth

        Paralyze ->
            Fifth

        PoisonField ->
            Fifth

        SummonCreature ->
            Fifth

        Dispel ->
            Sixth

        EnergyBolt ->
            Sixth

        Explosion ->
            Sixth

        Invisibility ->
            Sixth

        Mark ->
            Sixth

        MassCurse ->
            Sixth

        ParalyzeField ->
            Sixth

        Reveal ->
            Sixth

        ChainLightning ->
            Seventh

        EnergyField ->
            Seventh

        Flamestrike ->
            Seventh

        GateTravel ->
            Seventh

        ManaVampire ->
            Seventh

        MassDispel ->
            Seventh

        MeteorSwarm ->
            Seventh

        Polymorph ->
            Seventh

        Earthquake ->
            Eighth

        EnergyVortex ->
            Eighth

        Resurrection ->
            Eighth

        SummonAirElemental ->
            Eighth

        SummonDaemon ->
            Eighth

        SummonEarthElemental ->
            Eighth

        SummonFireElemental ->
            Eighth

        SummonWaterElemental ->
            Eighth


type Circle
    = First
    | Second
    | Third
    | Fourth
    | Fifth
    | Sixth
    | Seventh
    | Eighth


list : List Circle
list =
    [ First
    , Second
    , Third
    , Fourth
    , Fifth
    , Sixth
    , Seventh
    , Eighth
    ]


circleManaReq : Circle -> Int
circleManaReq circle =
    case circle of
        First ->
            4

        Second ->
            6

        Third ->
            9

        Fourth ->
            11

        Fifth ->
            14

        Sixth ->
            20

        Seventh ->
            40

        Eighth ->
            50


circleMageryReq : Circle -> Int
circleMageryReq circle =
    case circle of
        First ->
            1

        Second ->
            6

        Third ->
            16

        Fourth ->
            26

        Fifth ->
            36

        Sixth ->
            52

        Seventh ->
            66

        Eighth ->
            80


toString : Circle -> String
toString circle =
    case circle of
        First ->
            "First"

        Second ->
            "Second"

        Third ->
            "Third"

        Fourth ->
            "Fourth"

        Fifth ->
            "Fifth"

        Sixth ->
            "Sixth"

        Seventh ->
            "Seventh"

        Eighth ->
            "Eighth"



{-
   Formula taken from http://www.uorenaissance.com/info/?page=g_info_t2&id=225
-}


circleCastChance : Int -> Int -> Circle -> Float
circleCastChance mana skill circle =
    let
        avgSuccessPerPoint =
            2.5

        difference =
            skill - circleMageryReq circle

        chance =
            toFloat difference * avgSuccessPerPoint

        result =
            if chance > 100 then
                100
            else
                chance
    in
    if circleManaReq circle > mana then
        0
    else if difference > 0 then
        result
    else
        0


spellCastChance : Int -> Int -> Spell -> Float
spellCastChance mana skill spell =
    let
        circle =
            getSpellCircle spell
    in
    circleCastChance mana skill circle
